import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../User';
import { Channel } from '../Models/Channel';
import { ChannelService } from '../Services/channel.service';
import { NgForm } from '@angular/forms';
import swal from 'sweetalert';
import { Member } from '../Models/Member';
import { Router } from '@angular/router';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy{

  public user: User;
  public user1: User;
  public channels = [];
  public channel: Channel;
  public member: Member;

  constructor(private channelService: ChannelService, private router: Router) { }

  ngOnInit() {
    this.setUser();
    this.getChannels();
  }
  ngOnDestroy(){
    this.member = null;
    this.channel = null;
  }
  /*This methos permit set default user admin  */
  setUser(){
    this.user = new User('jarojasp@est.utn.ac.cr','123456');
    localStorage.setItem( 'user', JSON.stringify( this.user ) );
    this.user1 =  JSON.parse ( localStorage.getItem( 'user' ) );
    //console.log(this.user1);
  }

  //Get all channels in twilio
  getChannels(){
    this.channelService.getChannels().subscribe(data => {
      this.channels = data['channels'];
      localStorage.setItem( 'channels', JSON.stringify( this.channels ) ); 
      //console.log("Correcto");
      }, err => {
      console.log("Error en servicio get channels");
      });

      this.channels = JSON.parse ( localStorage.getItem( 'channels' ) );
  }
  loadData(channel: Channel){
    this.channel = channel;
  }

  joinToChannel(form: NgForm){

    if(form.value.identity != null && this.channel != null){
      this.channelService.joinMemberToChannel(this.channel, form.value.identity).subscribe(data => {
        this.member = data;
        console.log("join Correcto");
        localStorage.setItem( 'member', JSON.stringify(this.member));
        localStorage.setItem( 'channel', JSON.stringify(this.channel));
        this.router.navigate( ['../chat']);
      }, err => {
            console.log("Error join");
            swal({
              title: 'There was an error to join the channel',
              text:  'Member already exists',
              icon:  'error',
          });
      });

    } else{
      swal({
        title: 'Missing data',
        text:  'You need a friendly name to join the channel',
        icon:  'error',
    });

    }
  }

}
