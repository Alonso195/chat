import { Link } from "./Link";

export class Channel {
    unique_name?: any;
    members_count: number;
    date_updated: Date;
    friendly_name: string;
    created_by: string;
    account_sid: string;
    url: string;
    date_created: Date;
    sid: string;
    attributes: string;
    service_sid: string;
    type: string;
    messages_count: number;
    links: Link[];
}


