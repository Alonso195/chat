export class Member {
    channel_sid: string;
    date_updated: Date;
    last_consumption_timestamp?: any;
    account_sid: string;
    url: string;
    last_consumed_message_index?: any;
    role_sid: string;
    sid: string;
    date_created: Date;
    service_sid: string;
    identity: string;
    attributes: string;
}

