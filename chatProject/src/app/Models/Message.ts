export class Message {
    body: string;
    index: number;
    channel_sid: string;
    from: string;
    date_updated: Date;
    type: string;
    account_sid: string;
    to: string;
    last_updated_by?: any;
    date_created: Date;
    media?: any;
    sid: string;
    url: string;
    attributes: string;
    service_sid: string;
    was_edited: boolean;
}

