export class Link {
    webhooks: string;
    messages: string;
    invites: string;
    members: string;
    last_message?: any;
}