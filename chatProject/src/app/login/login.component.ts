import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../User';
import { Router } from '@angular/router';
import swal from 'sweetalert';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user : User;
  public userlog : User;


  constructor(private router: Router) { }

  ngOnInit() {
  }
  /*This methos is for login admin users
    receive a form as a parameter
  */
  login(form: NgForm) {
    this.user = JSON.parse(localStorage.getItem('user'));

    if (form.value.email == this.user.email && form.value.password == this.user.password){
      this.userlog = new User(form.value.email,form.value.password);
      localStorage.setItem('userlog', JSON.stringify(this.userlog));
      this.router.navigate( ['../viewChannel']);
      console.log('You are login');
    } else{
      swal({
        title: 'Error',
        text:  'email or password incorrect, please try again',
        icon:  'error',
    });

    }

  }

}
