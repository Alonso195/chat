import { Component, OnInit } from '@angular/core';
import { ChannelService } from '../Services/channel.service';
import { NgForm } from '@angular/forms';
import swal from 'sweetalert';
import { Channel } from '../Models/Channel';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-channels',
  templateUrl: './view-channels.component.html',
  styleUrls: ['./view-channels.component.css']
})
export class ViewChannelsComponent implements OnInit {

  public channels = [];
  public channel: Channel;
  
  constructor(private channelService: ChannelService, private router: Router) { }

  ngOnInit() {
    this.channel = new Channel;
    this.getChannels();
  }

  //Get all channels in twilio
  getChannels(){
    this.channelService.getChannels().subscribe(data => {
      this.channels = data['channels'];
      localStorage.setItem( 'channels', JSON.stringify( this.channels ) );
      
      //console.log("Correcto");
      }, err => {
      console.log("Error en servicio");
      });

      this.channels = JSON.parse ( localStorage.getItem( 'channels' ) );
      //console.log(this.channels);

  }
  // Create a new Channel s
  createChannel(form: NgForm){
    if(form.value.friendly_name){
      console.log(form.value.friendly_name);
      this.channelService.postChannel(form.value.friendly_name).subscribe(data => {
        console.log(data);
        console.log("Correcto");
        }, err => {
        console.log("Error en servicio");
        });

    } else {
      swal({
        title: 'Missing data',
        text:  'Please enter the Friendly Name',
        icon:  'warning',
    });
    }
    this.getChannels();
    this.ngOnInit();
    
  }
  // This method load data to the modal for delete
  loadData(channel: Channel){
    this.channel = channel;
  }

  //Delete a channel
  deleteChannel(sid: string){
    
    if(sid){
      this.channelService.deleteChannel(sid).subscribe(data => {
        console.log("Correcto");
        }, err => {
        console.log("Error en servicio");
        });

    } else {
      swal({
        title: 'Error',
        text:  'Please try again',
        icon:  'error',
    });
    }
    //this.getChannels();
    //this.ngOnInit();
    //window.location.reload();
  }
  //edit a channel
  editChannel(form: NgForm){
    if(form.value.friendly_name){
      this.channelService.editChannel(form.value.friendly_name, this.channel.sid).subscribe(data => {
        console.log("Correcto");
        }, err => {
        console.log("Error en servicio");
        });

    } else {
      swal({
        title: 'Error',
        text:  'Please try again',
        icon:  'error',
    });
    }
    window.location.reload();
    //this.router.navigate( ['../main']);
    //this.getChannels();
    //this.ngOnInit();
  }

}
