import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  header: HttpHeaders;
  public urlBase = 'https://chat.twilio.com/v2/Services/';

  constructor() { 
    this.header = new HttpHeaders({Authorization: 'Basic ' + btoa('ACa42a35d5d0f58a906fe8cfcd3b66baa3:0e832571e5d98c0167f22e016ca00618')});
    this.header.append('Content-Type','application/x-www-form-urlencoded');
    this.setUrlBase();
  }
  //Set SID in urlBase
  setUrlBase(){
    this.urlBase += 'IS7bcfbd0b7b9d4ddb821f7252c3fc0840';
  }
  //return httpHeaders
  getHeader(){
    return {headers: this.header};
  }
}
