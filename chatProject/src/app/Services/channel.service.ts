import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Channel } from '../Models/Channel';
import { HeaderService } from './header.service';
import { Member } from '../Models/Member';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

const httpOptions = {
  headers: new HttpHeaders({
     'Content-Type': 'application/json',
     'Authorization': 'basic'
    })
};

@Injectable({
  providedIn: 'root'
})
export class ChannelService {

  private channelUrl: string; // base url
 
  constructor(private http: HttpClient, private headerService: HeaderService) { 
    this.channelUrl = this.headerService.urlBase + '/Channels';
  }

  getChannels(): Observable<Channel[]> {
    this.channelUrl = null;
    this.channelUrl = this.headerService.urlBase + '/Channels';
    return this.http.get<Channel[]>(this.channelUrl, this.headerService.getHeader());
  }

  postChannel(friendly_name: string){
    const body = new HttpParams() 
    .set('FriendlyName', friendly_name); 
    return this.http.post<Channel[]>(this.channelUrl, body, this.headerService.getHeader());
  }
  deleteChannel(sid: string){
    this.channelUrl += '/';
    this.channelUrl  +=sid
    return this.http.delete<Channel[]>(this.channelUrl, this.headerService.getHeader());
  }

  editChannel(friendly_name: string, sid: string): Observable<Channel> {
    this.channelUrl += '/';
    this.channelUrl  +=sid;
    const body = new HttpParams() 
    .set('FriendlyName', friendly_name); 
    return this.http.post<Channel>(this.channelUrl, body, this.headerService.getHeader());
  }

  joinMemberToChannel(channel: Channel, identity: string): Observable<Member>{
    this.channelUrl += '/';
    this.channelUrl  += channel.sid;
    this.channelUrl += '/Members';
    const body = new HttpParams() 
    .set('Identity', identity); 
    return this.http.post<Member>(this.channelUrl, body, this.headerService.getHeader());
  }

  deleteMember(member: Member){
    this.channelUrl = null;
    this.channelUrl = this.headerService.urlBase + '/Channels';
    this.channelUrl += '/';
    this.channelUrl  += member.channel_sid;
    this.channelUrl += '/Members/';
    this.channelUrl += member.identity
    console.log(this.channelUrl);
    
    return this.http.delete<Channel>(this.channelUrl, this.headerService.getHeader());
  }
  // return list of members to especific channel
  getMembers(member: Member): Observable<Member[]> {
    this.channelUrl = null;
    this.channelUrl = this.headerService.urlBase + '/Channels';
    this.channelUrl += '/';
    this.channelUrl  += member.channel_sid;
    this.channelUrl += '/Members/';
    return this.http.get<Member[]>(this.channelUrl, this.headerService.getHeader());
  }

  // return list of messages to especific channel
  getMessages(member: Member): Observable<Message[]> {
    this.channelUrl = null;
    this.channelUrl = this.headerService.urlBase + '/Channels';
    this.channelUrl += '/';
    this.channelUrl  += member.channel_sid;
    this.channelUrl += '/Messages';
    return this.http.get<Message[]>(this.channelUrl, this.headerService.getHeader());
  }

  sendMessage(member: Member, message: string): Observable<Message>{
    this.channelUrl = null;
    this.channelUrl = this.headerService.urlBase + '/Channels';
    this.channelUrl += '/';
    this.channelUrl  += member.channel_sid;
    this.channelUrl += '/Messages';

    const body = new HttpParams()
    .set('Body', message)
    .set('From', member.identity);
    return this.http.post<Message>(this.channelUrl, body, this.headerService.getHeader());
  }
}
