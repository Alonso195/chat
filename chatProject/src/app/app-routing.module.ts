import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewChannelsComponent } from './view-channels/view-channels.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { ChatComponent } from './chat/chat.component';

const routes: Routes = [
  { path: 'main', component: MainComponent },
  { path: 'viewChannel', component: ViewChannelsComponent },
  { path: 'chat', component: ChatComponent },
  { path: 'login', component: LoginComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'main' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
