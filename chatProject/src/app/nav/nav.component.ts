import { Component, OnInit } from '@angular/core';
import { User } from '../User';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public userlog: User;
  constructor() { }

  ngOnInit() {
    this.getUserLog();
  }
  /*This method lets you know which user is login to know which nav to show*/
  getUserLog(){
    this.userlog = JSON.parse(localStorage.getItem('userlog'));
  }
  /* this methos lets logout to the aplication */
  logout(){
    localStorage.removeItem('userlog');
    this.userlog = null;
  }

}
