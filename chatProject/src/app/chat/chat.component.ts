import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ChannelService } from '../Services/channel.service';
import { Member } from '../Models/Member';
import { Channel } from '../Models/Channel';
import { NgForm } from '@angular/forms';
import { Message } from '../Models/Message';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy  {

  public member: Member;
  public channel: Channel;
  public members = [];
  public messages = [];

  constructor(private channelService: ChannelService, private router: Router) { }

  ngOnInit() {
    this.getMember();
    this.getMembers();
    this.getMessages();
  }

  ngOnDestroy(){
    this.deleteMember();
    this.deletelistOfMembers();
  }

  getMember(){
    this.member = JSON.parse ( localStorage.getItem('member'));
    this.channel = JSON.parse ( localStorage.getItem('channel'));
  }

  deleteMember(){
    this.channelService.deleteMember(this.member).subscribe(data => {
      console.log("delete Correcto");
    }, err => {
          console.log("Error join");
    });
    localStorage.removeItem('member');
    this.member = null;
  }

  deletelistOfMembers(){
    localStorage.removeItem('members');
    this.members = null;
  }

  getMembers(){
    this.channelService.getMembers(this.member).subscribe(data => {
      this.members = data['members'];
      localStorage.setItem( 'members', JSON.stringify( this.members ) ); 
      //console.log("Correcto");
      }, err => {
      console.log("Error en servicio get members");
      });
      this.members = JSON.parse ( localStorage.getItem( 'members' ) );
     // console.log(this.members);
  }

  getMessages(){

    this.channelService.getMessages(this.member).subscribe(data => {
      this.messages = data['messages'];
      localStorage.setItem( 'messages', JSON.stringify( this.messages ) ); 
      //console.log("Correcto");
      }, err => {
      console.log("Error en servicio get messages");
      });
      this.messages = JSON.parse ( localStorage.getItem( 'messages' ) );
      //console.log(this.messages);
  }

  sendMessage(form: NgForm){
    if(form.value.message){
      this.channelService.sendMessage(this.member, form.value.message).subscribe(data => {
      
        console.log("Correcto");
        }, err => {
        console.log("Error en servicio get messages");
        });
    }
  }

  

}
